import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes} from '@angular/router';
import { HttpModule } from '@angular/http';
import { GoogleChartsModule } from 'angular-google-charts';

import { AppComponent } from './app.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { FactorySelectionComponent } from './components/factory-selection/factory-selection.component';
import { PanelComponent } from './components/panel/panel.component';
import { TabPaneSublimeComponent } from './components/tab-pane-sublime/tab-pane-sublime.component';
import { TabPaneAnantaComponent } from './components/tab-pane-ananta/tab-pane-ananta.component';
import { EndlineQcComponent } from './components/endline-qc/endline-qc.component';
import { SewInlineComponent } from './components/sew-inline/sew-inline.component';
import { LineInComponent } from './components/line-in/line-in.component';
import { LineIn2Component } from './components/line-in2/line-in2.component';

import { DataService } from './services/data.service';
import { AuthService } from './services/auth.service';
import { EndlineQcSublimeComponent } from './components/endline-qc-sublime/endline-qc-sublime.component';
import { LineInSublimeComponent } from './components/line-in-sublime/line-in-sublime.component';
import { PackingInSublimeComponent } from './components/packing-in-sublime/packing-in-sublime.component';
import { HomeComponent } from './components/home/home.component';
import { TimeEventComponent } from './components/time-event/time-event.component';
import { TimeEventChartComponent } from './components/time-event-chart/time-event-chart.component';
import { TimeEventSearchComponent } from './components/time-event-search/time-event-search.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LineDetailPopupComponent } from './components/line-detail-popup/line-detail-popup.component';
import { FirstEventComponent } from './components/first-event/first-event.component';
import { FinInlineComponent } from './components/fin-inline/fin-inline.component';
import { FinAqlComponent } from './components/fin-aql/fin-aql.component';
import { FinEndlineComponent } from './components/fin-endline/fin-endline.component';
import { SewingAqlComponent } from './components/sewing-aql/sewing-aql.component';

const appRoutes: Routes = [

  {path:'', component: HomeComponent},
  {path:'time_event', component: TimeEventComponent},

];


@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    BodyComponent,
    FooterComponent,
    FactorySelectionComponent,
    PanelComponent,
    TabPaneSublimeComponent,
    TabPaneAnantaComponent,
    EndlineQcComponent,
    SewInlineComponent,
    LineInComponent,
    LineIn2Component,
    EndlineQcSublimeComponent,
    LineInSublimeComponent,
    PackingInSublimeComponent,
    HomeComponent,
    TimeEventComponent,
    TimeEventChartComponent,
    TimeEventSearchComponent,
    SidebarComponent,
    LineDetailPopupComponent,
    FirstEventComponent,
    FinInlineComponent,
    FinAqlComponent,
    FinEndlineComponent,
    SewingAqlComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    GoogleChartsModule.forRoot()
  ],
  providers: [
    DataService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
