import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http:Http
  ) { }

  getFirstEvent(line, factory, date, hsk, at){
    return this.http.get('/monitoring-app-api/events/get_first_event?line=' + line + '&factory=' + factory + '&date=' + date + '&hsk=' + hsk + '&at=' + at).pipe(map(res => res.json()));  
  }

  getTimeEvent(index, line){
    return this.http.get('/monitoring-app-api/events/get_time_event?line=' + line + '&index=' + index).pipe(map(res => res.json()));  
  }

  getAllIndexes(){
    return this.http.get('/monitoring-app-api/events/get_all_indexes').pipe(map(res => res.json()));  
  }

  getAllSubjectsForIndex(index){
    return this.http.get('/monitoring-app-api/events/get_all_subjects_for_index?index=' + index).pipe(map(res => res.json())); 
  }

  getAllFirstEvents(factory,date){
    return this.http.get('/monitoring-app-api/events/get_all_first_events?factory=' + factory +'&date='+ date).pipe(map(res => res.json())); 
  }

}
