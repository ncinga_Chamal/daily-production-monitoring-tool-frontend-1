import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { LINES } from '../../classes/lines';
import { Line } from '../../classes/line';
import { FACTORIES } from '../../classes/factories';
import { Factory } from '../../classes/factory';

@Component({
  selector: 'app-time-event-search',
  templateUrl: './time-event-search.component.html',
  styleUrls: ['./time-event-search.component.css']
})
export class TimeEventSearchComponent implements OnInit {

  selectedFactory: Factory;
  factories = FACTORIES;

  selectedLine: Line;
  lines = LINES;

  selected_index:String;
  selected_subject:String;

  indexes = [];
  subjects = [];

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) {

    this.dataService.currentIndexSelectionValue.subscribe(data => {
      this.getSubjectsForIndex();
    });

  }

  ngOnInit() {
    //console.log(this.selected_line);
    this.authService.getAllIndexes().subscribe(data => {

      for (var i = 0; i < data.aggregations[1].buckets.length; i++) {
        this.indexes.push(data.aggregations[1].buckets[i].key);
      }
      
    });

  }

  onChangeSwitchIndex(){
    this.dataService.changeIndex(this.selected_index);
  }

  onChangeSwitchSubject(){
    this.dataService.changeSubject(this.selected_subject);
  }

  getSubjectsForIndex(){

    this.authService.getAllSubjectsForIndex(this.selected_index).subscribe(data => {

      this.subjects.length = 0;

      for (var i = 0; i < data.aggregations[1].buckets.length; i++) {
        this.subjects.push(data.aggregations[1].buckets[i].key);
      }

    });

  }

}
