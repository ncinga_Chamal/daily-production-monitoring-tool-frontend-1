import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-fin-aql',
  templateUrl: './fin-aql.component.html',
  styleUrls: ['./fin-aql.component.css']
})
export class FinAqlComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  fin_aql:Station[] = [
    {
        at:"fin_aql",
        sub: "floor1-finishing-section1-line1",
        sub_ex:"ant-aal-aepz-floor1-finishing-section1-line1",
        dis_name: "TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section1-line2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line2",
        dis_name: "TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section1-line3",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line3",
        dis_name: "TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section1-line4",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line4",
        dis_name: "TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section2-line5",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line5",
        dis_name: "TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section2-line6",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line6",
        dis_name: "TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section2-line7",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line7",
        dis_name: "TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section2-line8",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line8",
        dis_name: "TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_aql",
        sub: "floor1-finishing-section2-line9",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line9",
        dis_name: "TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) {

  }

  ngOnInit() { 
    
  }

  ngOnChanges() {

    for(var i=0;i<this.fin_aql.length;i++){

      this.fin_aql[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){

      for(var i=0;i<this.fin_aql.length;i++){

        for(var j=0;j<this.subject_array.length;j++){
  
          if(this.subject_array[j].at == 'fin_aql' && this.subject_array[j].sub == this.fin_aql[i].sub_ex && this.subject_array[j].hsk == this.fin_aql[i].hsk){
  
            this.fin_aql[i].f_event = "green-palatte";
  
          }
        
        }
  
      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
