import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station'; 

@Component({
  selector: 'app-sew-inline',
  templateUrl: './sew-inline.component.html',
  styleUrls: ['./sew-inline.component.css']
})
export class SewInlineComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  sew_inline:Station[] = [
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line1-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line1-station1",
        dis_name: "Front-TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line1-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line1-station2",
        dis_name: "Back-TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line1-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line1-station3",
        dis_name: "Waist-TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line2-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line2-station1",
        dis_name: "Front-TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line2-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line2-station2",
        dis_name: "Back-TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line2-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line2-station3",
        dis_name: "Waist-TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line3-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line3-station1",
        dis_name: "Front-TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line3-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line3-station2",
        dis_name: "Back-TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line3-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line3-station3",
        dis_name: "Waist-TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line4-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line4-station1",
        dis_name: "Front-TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line4-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line4-station2",
        dis_name: "Back-TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section1-line4-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line4-station3",
        dis_name: "Waist-TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line5-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line5-station1",
        dis_name: "Front-TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line5-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line5-station2",
        dis_name: "Back-TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line5-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line5-station3",
        dis_name: "Waist-TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line6-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line6-station1",
        dis_name: "Front-TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line6-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line6-station2",
        dis_name: "Back-TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line6-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line6-station3",
        dis_name: "Waist-TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line7-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line7-station1",
        dis_name: "Front-TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line7-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line7-station2",
        dis_name: "Back-TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line7-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line7-station3",
        dis_name: "Waist-TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line8-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line8-station1",
        dis_name: "Front-TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line8-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line8-station2",
        dis_name: "Back-TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line8-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line8-station3",
        dis_name: "Waist-TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line9-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line9-station1",
        dis_name: "Front-TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line9-station2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line9-station2",
        dis_name: "Back-TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"sew_inline",
        sub: "floor1-sewing-section2-line9-station3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line9-station3",
        dis_name: "Waist-TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    },
  ];

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

  }

  ngOnChanges() {

    for(var i=0;i<this.sew_inline.length;i++){

        this.sew_inline[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){
        
        for(var i=0;i<this.sew_inline.length;i++){
            
            for(var j=0;j<this.subject_array.length;j++){
                
                if(this.subject_array[j].at == 'sew_inline' && this.subject_array[j].sub == this.sew_inline[i].sub_ex && this.subject_array[j].hsk == this.sew_inline[i].hsk){
                    
                    this.sew_inline[i].f_event = "green-palatte";
                } 

            }

        }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
