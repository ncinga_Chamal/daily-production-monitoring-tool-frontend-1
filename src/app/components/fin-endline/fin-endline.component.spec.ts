import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinEndlineComponent } from './fin-endline.component';

describe('FinEndlineComponent', () => {
  let component: FinEndlineComponent;
  let fixture: ComponentFixture<FinEndlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinEndlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinEndlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
