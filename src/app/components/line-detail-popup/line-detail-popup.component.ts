import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service'; 
import { Station } from '../../classes/station';

@Component({
  selector: 'app-line-detail-popup',
  templateUrl: './line-detail-popup.component.html',
  styleUrls: ['./line-detail-popup.component.css']
})
export class LineDetailPopupComponent implements OnInit {

  @Input() station: Station;

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
    this.dataService.currentStationSelectionValue.subscribe(value => {
      this.station = value;
    });
  }

}
