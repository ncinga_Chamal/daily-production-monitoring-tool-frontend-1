import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-factory-selection',
  templateUrl: './factory-selection.component.html',
  styleUrls: ['./factory-selection.component.css']
})
export class FactorySelectionComponent implements OnInit {

  factory_btn_class = {
    ananta: "btn btn-info active",
    sublime: "btn btn-default"
  }

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
  }

  onClickFactorySelection(factory){
    this.dataService.changeFactoryPane(factory);
    if (factory == 'ananta'){
      this.factory_btn_class.ananta = "btn btn-info active";
      this.factory_btn_class.sublime = "btn btn-default";
    } else {
      this.factory_btn_class.sublime = "btn btn-info active";
      this.factory_btn_class.ananta = "btn btn-default";
    }
  }

}
