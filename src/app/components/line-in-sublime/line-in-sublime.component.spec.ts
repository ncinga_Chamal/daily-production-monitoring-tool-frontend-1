import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineInSublimeComponent } from './line-in-sublime.component';

describe('LineInSublimeComponent', () => {
  let component: LineInSublimeComponent;
  let fixture: ComponentFixture<LineInSublimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineInSublimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineInSublimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
