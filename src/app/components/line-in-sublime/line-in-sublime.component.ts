import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station'; 

@Component({
  selector: 'app-line-in-sublime',
  templateUrl: './line-in-sublime.component.html',
  styleUrls: ['./line-in-sublime.component.css']
})
export class LineInSublimeComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  line_in:Station[] = [
    {
        at:"line-in",
        sub: "floor3-sewing-line1",
        sub_ex: "meg-sgt-floor3-sewing-line1",
        dis_name: "K01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line2",
        sub_ex: "meg-sgt-floor3-sewing-line2",
        dis_name: "K02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line3",
        sub_ex: "meg-sgt-floor3-sewing-line3",
        dis_name: "K03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line4",
        sub_ex: "meg-sgt-floor3-sewing-line4",
        dis_name: "K04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line5",
        sub_ex: "meg-sgt-floor3-sewing-line5",
        dis_name: "K05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line6",
        sub_ex: "meg-sgt-floor3-sewing-line6",
        dis_name: "K06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line1",
        sub_ex: "meg-sgt-floor3-sewing-line1",
        dis_name: "K01",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line2",
        sub_ex: "meg-sgt-floor3-sewing-line2",
        dis_name: "K02",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line3",
        sub_ex: "meg-sgt-floor3-sewing-line3",
        dis_name: "K03",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line4",
        sub_ex: "meg-sgt-floor3-sewing-line4",
        dis_name: "K04",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line5",
        sub_ex: "meg-sgt-floor3-sewing-line5",
        dis_name: "K05",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"line-in",
        sub: "floor3-sewing-line6",
        sub_ex: "meg-sgt-floor3-sewing-line6",
        dis_name: "K06",
        hsk: "secondary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {
    
  }

  ngOnChanges() {
      
    for(var i=0;i<this.line_in.length;i++){

        this.line_in[i].f_event = "red-palatte";
        
    }
    

    if(this.subject_array.length != 0){
        
        for(var i=0;i<this.line_in.length;i++){
            
            for(var j=0;j<this.subject_array.length;j++){
                
                if(this.subject_array[j].at == 'line_in' && this.subject_array[j].sub == this.line_in[i].sub_ex && this.subject_array[j].hsk == this.line_in[i].hsk){
                    
                    this.line_in[i].f_event = "green-palatte";

                }
        
            }

        }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
