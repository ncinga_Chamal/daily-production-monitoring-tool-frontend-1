import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { App } from '../../classes/app';
import { APPS } from '../../classes/apps';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  apps = APPS;

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
    
    //console.log(this.factory);
  }

  setEventDate(date){
    this.dataService.formatDateAndChangeSource(date);
  }

}
