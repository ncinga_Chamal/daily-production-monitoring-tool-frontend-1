import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-endline-qc-sublime',
  templateUrl: './endline-qc-sublime.component.html',
  styleUrls: ['./endline-qc-sublime.component.css']
})
export class EndlineQcSublimeComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  endline_qc:Station[] = [
    {
        at:"endline_qc",
        sub: "floor3-sewing-line1",
        sub_ex: "meg-sgt-floor3-sewing-line1",
        dis_name: "K01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line2",
        sub_ex: "meg-sgt-floor3-sewing-line2",
        dis_name: "K02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line3",
        sub_ex: "meg-sgt-floor3-sewing-line3",
        dis_name: "K03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line4",
        sub_ex: "meg-sgt-floor3-sewing-line4",
        dis_name: "K04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line5",
        sub_ex: "meg-sgt-floor3-sewing-line5",
        dis_name: "K05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line6",
        sub_ex: "meg-sgt-floor3-sewing-line6",
        dis_name: "K06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line1",
        sub_ex: "meg-sgt-floor3-sewing-line1",
        dis_name: "K01",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line2",
        sub_ex: "meg-sgt-floor3-sewing-line2",
        dis_name: "K02",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line3",
        sub_ex: "meg-sgt-floor3-sewing-line3",
        dis_name: "K03",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line4",
        sub_ex: "meg-sgt-floor3-sewing-line4",
        dis_name: "K04",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line5",
        sub_ex: "meg-sgt-floor3-sewing-line5",
        dis_name: "K05",
        hsk: "secondary",
        f_event:"red-palatte"
    },
    {
        at:"endline_qc",
        sub: "floor3-sewing-line6",
        sub_ex: "meg-sgt-floor3-sewing-line6",
        dis_name: "K06",
        hsk: "secondary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

  }

  ngOnChanges() {

    for(var i=0;i<this.endline_qc.length;i++){

        this.endline_qc[i].f_event = "red-palatte";
        
    }

    if(this.subject_array.length != 0){

      for(var i=0;i<this.endline_qc.length;i++){

        for(var j=0;j<this.subject_array.length;j++){

          if(this.subject_array[j].at == 'endline_qc' && this.subject_array[j].sub == this.endline_qc[i].sub_ex && this.subject_array[j].hsk == this.endline_qc[i].hsk){

            this.endline_qc[i].f_event = "green-palatte";

          }
        
        }

      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
