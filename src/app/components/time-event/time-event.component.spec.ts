import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeEventComponent } from './time-event.component';

describe('TimeEventComponent', () => {
  let component: TimeEventComponent;
  let fixture: ComponentFixture<TimeEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
