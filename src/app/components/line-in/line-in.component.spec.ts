import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineInComponent } from './line-in.component';

describe('LineInComponent', () => {
  let component: LineInComponent;
  let fixture: ComponentFixture<LineInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
