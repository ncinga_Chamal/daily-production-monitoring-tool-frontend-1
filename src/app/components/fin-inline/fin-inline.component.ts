import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-fin-inline',
  templateUrl: './fin-inline.component.html',
  styleUrls: ['./fin-inline.component.css']
})
export class FinInlineComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  fin_inline:Station[] = [
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line1-station1",
        sub_ex:"ant-aal-aepz-floor1-finishing-section1-line1-station1",
        dis_name: "FIN-INSIDE-TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line1-station2",
        sub_ex:"ant-aal-aepz-floor1-finishing-section1-line1-station2",
        dis_name: "FIN-TOPSIDE-TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line2-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line2-station1",
        dis_name: "FIN-INSIDE-TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line2-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line2-station2",
        dis_name: "FIN-TOPSIDE-TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line3-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line3-station1",
        dis_name: "FIN-INSIDE-TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line3-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line3-station2",
        dis_name: "FIN-TOPSIDE-TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line4-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line4-station1",
        dis_name: "FIN-INSIDE-TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section1-line4-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section1-line4-station2",
        dis_name: "FIN-TOPSIDE-TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line5-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line5-station1",
        dis_name: "FIN-INSIDE-TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line5-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line5-station2",
        dis_name: "FIN-TOPSIDE-TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line6-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line6-station1",
        dis_name: "FIN-INSIDE-TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line6-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line6-station2",
        dis_name: "FIN-TOPSIDE-TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line7-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line7-station1",
        dis_name: "FIN-INSIDE-TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line7-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line7-station2",
        dis_name: "FIN-TOPSIDE-TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line8-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line8-station1",
        dis_name: "FIN-INSIDE-TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line8-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line8-station2",
        dis_name: "FIN-TOPSIDE-TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line9-station1",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line9-station1",
        dis_name: "FIN-INSIDE-TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"fin_inline",
        sub: "floor1-finishing-section2-line9-station2",
        sub_ex: "ant-aal-aepz-floor1-finishing-section2-line9-station2",
        dis_name: "FIN-TOPSIDE-TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) {

  }

  ngOnInit() { 
    
  }

  ngOnChanges() {

    for(var i=0;i<this.fin_inline.length;i++){

      this.fin_inline[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){

      for(var i=0;i<this.fin_inline.length;i++){

        for(var j=0;j<this.subject_array.length;j++){
  
          if(this.subject_array[j].at == 'fin_inline' && this.subject_array[j].sub == this.fin_inline[i].sub_ex && this.subject_array[j].hsk == this.fin_inline[i].hsk){
  
            this.fin_inline[i].f_event = "green-palatte";
  
          }
        
        }
  
      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
